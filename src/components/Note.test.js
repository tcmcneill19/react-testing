import React from 'react';
import {mount, configure, render} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16'
import Note from './Note';



configure ({adapter: new Adapter()});

const props = {note: {text:'test note'}}

console.log({...props});// spreading the atttributes of the obj into the components

// const triplePrint = (a, b,c) => {
//     console.log(`${a} ${b } ${c}`);
// };

// const message = ['react', 'is', 'awesome'];

// triplePrint('react', 'is', 'awesome');
// triplePrint(message[0], message[1], message[2]);// same as the previous statement
// triplePrint(...message); //spread operator makes this shorter but delievers the same message

describe('Note', () =>{
  let note = mount(<Note {...props}/>);  

  it('renders the note text', () => {
      console.log (note.debug());
      expect(note.find('p').text()).toEqual(props.note.text);// spread operator(...)-- spread the attibutes of the note
  });
});